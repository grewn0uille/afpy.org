=================
Qui sommes-nous ?
=================


L’association
=============

L’association « A.F.P.Y. » , Association Francophone Python, fondée le 11
décembre 2004 sous le régime de la loi du 1er juillet 1901 a pour but la
vulgarisation auprès d’un public francophone du langage de programmation python
et de ses applications.


Bureau et Comité Directeur
==========================

Les membres du comité directeur 2023 sont :

- Présidente : Lucie Anglade (president@afpy.org)
- Jean Lapostolle (Vice-président)
- Antoine Rozo (secretaire@afpy.org)
- Nicolas Ledez (Vice-secrétaire)
- Pierre Bousquié (tresorier@afpy.org)
- Thomas Bouchet (Vice-trésorier)
- Marc Debureaux
- Laurine Leulliette
- Bruno Bonfils
